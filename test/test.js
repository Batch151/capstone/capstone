const chai = require('chai');
const {expect} = require('chai');
const http = require('chai-http');

chai.use(http);

describe('API Test Suite', () => {
    it('Test Post Currency is Running', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": "sample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200)
            done();
        })
    });
    it('Test Post Currency returns 400 if Name is Missing', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if Name is not a string', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": 123,
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if Name is an Empty String', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": "",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if EX is Missing', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": "sample"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency return 400 if EX is not an object', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": "sample",
            "ex": "not an object"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency return 400 if EX is an Empty Object', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "forTest",
            "name": "sample",
            "ex": {}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if ALIAS is Missing', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "name": "sample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if ALIAS is Not a String', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": 123,
            "name": "sample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if ALIAS is an Empty String', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "",
            "name": "sample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 400 if there is a duplicate alias', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "dollars",
            "name": "sample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    });
    it('Test Post Currency returns 200 if fields are complete and no duplicates', (done) => {
        chai.request('http://localhost:4000')
        .post('/currency')
        .type('json')
        .send({
            "alias": "notDuplicate",
            "name": "otherSample",
            "ex": {
                "peso": 100,
                "won": 100,
                "yen": 100,
                "yuan": 100
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200)
            done();
        })
    });
})