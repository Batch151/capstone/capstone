const express = require('express');
const app = express();
const port = 4000;

app.use(express.json());

const exchangeRates = {
    'usd': {
        'alias' : 'dollars',
        'name': 'United States Dollar',
      	'ex': {
        	'peso': 50.73,
        	'won': 1187.24,
        	'yen': 108.63,
        	'yuan': 7.03
    	}
    },
    'yen': {
        'alias' : 'yen',
      	'name': 'Japanese Yen',
      	'ex': {
	        'peso': 0.47,
	        'usd': 0.0092,
	        'won': 10.93,
	        'yuan': 0.065
      	}
    },
    'peso': {
        'alias' : 'peso',
      	'name': 'Philippine Peso',
      	'ex':{
	        'usd': 0.020,
	        'won': 23.39,
	        'yen': 2.14,
	        'yuan': 0.14
      	}
    },
    'yuan': {
        'alias' : 'yuan',
      	'name': 'Chinese Yuan',
      	'ex': {
	        'peso': 7.21,
	        'usd': 0.14,
	        'won': 168.85,
	        'yen': 15.45
      	}
    },
    'won': {
        'alias' : 'won',
      	'name': 'South Korean Won',
      	'ex': {
	        'peso': 0.043,
	        'usd': 0.00084,
	        'yen': 0.092,
	        'yuan': 0.0059
      	}
    }
};

app.post('/currency', (req, res) => {
    if(!req.body.hasOwnProperty('name')) {
        return res.status(400).send({
            "Bad Request": "Missing required parameter NAME"
        })
    };
    if(typeof req.body.name !== 'string') {
        return res.status(400).send({
            "Bad Request": "NAME value is not a String"
        })
    };
    if(req.body.name.length === 0) {
        return res.status(400).send({
            "Bad Request": "NAME value is empty"
        })
    };
    if(!req.body.hasOwnProperty('ex')) {
        return res.status(400).send({
            "Bad Request": "Missing required paramater EX"
        })
    };
    if(typeof req.body.ex !== 'object') {
        return res.status(400).send({
            "Bad Request": "EX value is not a Object"
        })
    };
    if(Object.keys(req.body.ex).length === 0) {
        return res.status(400).send({
            "Bad Request": "EX value is empty"
        })
    };
    if(!req.body.hasOwnProperty('alias')) {
        return res.status(400).send({
            "Bad Request": "Missing required parameter ALIAS"
        })
    };
    if(typeof req.body.alias !== 'string') {
        return res.status(400).send({
            "Bad Request": "ALIAS value is not a String"
        })
    };
    if(req.body.alias.length === 0) {
        return res.status(400).send({
            "Bad Request": "ALIAS value is empty"
        })
    };
    let aliases = []
    for (const key in exchangeRates) {
        aliases.push(exchangeRates[key].alias)
    }
    if(aliases.some(alias => alias === req.body.alias)) {
        return res.status(400).send({
            "Bad Request": "Currency Alias Already Exist"
        })
    }
    return res.status(200).send("post successful");
})

app.listen(port, () => console.log(`Server successfully running on port ${port}`));